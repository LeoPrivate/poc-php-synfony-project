<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


class HomeController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('pages/home.html.twig', [
            'DOT_ENV_VARIABLE' => $_ENV['DOT_ENV_VARIABLE'],
            'ALL_GROUPVARS_DEV_VARIABLE' => $_ENV['ALL_GROUPVARS_DEV_VARIABLE'],
            'GROUP1_GROUPVARS_DEV_VARIABLE' => $_ENV['GROUP1_GROUPVARS_DEV_VARIABLE'],
            'SHARED_PROJECT_VARIABLE' => $_ENV['SHARED_PROJECT_VARIABLE'],
            'SHARED_EXTERNAL_API_VARIABLE_1' => $_ENV['SHARED_EXTERNAL_API_VARIABLE_1'],
            'SHARED_EXTERNAL_API_VARIABLE_2' => $_ENV['SHARED_EXTERNAL_API_VARIABLE_2']
        ]);
    }
}