<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class SpecificToThisEnv extends AbstractController {


    public function index(): Response {
        return $this->render('pages/specific-to-this-env.html.twig');
    }
}