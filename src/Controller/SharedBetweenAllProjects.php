<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class SharedBetweenAllProjects extends AbstractController {


    public function index(): Response {
        $test = $_ENV['DOT_ENV_VARIABLE'];

        return $this->render('pages/shared-between-all-projects.html.twig', [$test]);
    }
}